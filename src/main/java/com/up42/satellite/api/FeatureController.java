package com.up42.satellite.api;

import com.up42.satellite.model.Feature;
import com.up42.satellite.model.SatelliteImage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/v1/features")
public class FeatureController {

    @GetMapping("/")
    public List<Feature> getAllFeatures(){
        return Collections.emptyList();
    }


    @GetMapping("/{id}")
    public Feature getFeatureById(@PathVariable String id){
        return null;
    }

    @GetMapping("/{id}/quicklook")
    public SatelliteImage getSatelliteImage(@PathVariable String id){
        return null;
    }

}
