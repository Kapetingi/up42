package com.up42.satellite;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class IntegrationTests {


    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getFeatureTest() throws Exception {
        mockMvc.perform(get("/api/v1/features/39c2f29e-c0f8-4a39-a98b-deed547d6aea"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("39c2f29e-c0f8-4a39-a98b-deed547d6aea"))
                .andExpect(jsonPath("$.timestamp").value(1554831167697L))
                .andExpect(jsonPath("$.beginViewingDate").value(1554831167697L))
                .andExpect(jsonPath("$.endViewingDate").value(1554831202043L))
                .andExpect(jsonPath("$.missionName").value("Sentinel-1b"))
                .andReturn();
    }

}
